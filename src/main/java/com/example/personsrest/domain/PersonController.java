package com.example.personsrest.domain;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
@AllArgsConstructor
public class PersonController {

    PersonRepository personRepository;

    @GetMapping("/{id}")
    public PersonDTO getById(@PathVariable("id") String id) {
        return personRepository.findById(id).map(person -> toDTO(person))
                .orElse(null);
    }

    @GetMapping("/")
    public List<PersonDTO> getAll() {
        return personRepository.findAll().stream().map(person -> toDTO(person)).collect(Collectors.toList());
    }

    PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getAge(),
                person.getCity(),
                person.getGroups(),
                person.isActive());
    }
}