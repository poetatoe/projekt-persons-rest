package com.example.personsrest.domain;

import lombok.Value;

import java.util.List;

@Value
public class PersonDTO {
    String id;
    String name;
    int age;
    String city;
    List<String> groups;
    boolean isActive;

}
